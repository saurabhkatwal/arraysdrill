function cb(element) {
    element = String(element);
    if (element.length === 1) {
        return Number(element);
    } else if (element.length > 1) {
        let str = '';
        for (let i = 0; i < element.length; i++) {
            if (Number(element[i]) != NaN) {
                str += element[i];
            }
        }
        return Number(str);
    }
}

function flatten(elements) {
    let temp = [];
    for (let i = 0; i < elements.length; i++) {
        temp.push(cb(elements[i]));
    }
    return temp;
}
let numbers=[1,[2],[[3]],[[[4]]]];
let result=flatten(numbers);
console.log(result);