let numbers = [1, 2, 3, 4, 5, 6];

function map(elements, cb) {
    let temp = [];
    for (let i = 0; i < elements.length; i++) {
        temp.push(cb(elements[i], i));
    }
    return temp;
}

function myfunction(element, index) {
    return element * 2;
}
let newArray = map(numbers, myfunction);
console.log(newArray);