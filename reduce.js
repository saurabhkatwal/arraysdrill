let numbers = [1, 20, 31, 4, 15];

function reduce(elements, cb, startingValue) {
    if (startingValue === undefined) {
        startingValue = elements[0];
        i = 1;
    } else {
        i = 0;
    }
    while (i < elements.length) {
        startingValue = cb(startingValue, elements[i]);
        i++;
    }
    return startingValue;
}

function myFunction(startingValue, element) {
    startingValue += element;
    return startingValue;
}
let total = reduce(numbers, myFunction, 0);
console.log(total);